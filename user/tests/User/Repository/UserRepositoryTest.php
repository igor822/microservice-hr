<?php declare(strict_types=1);

namespace Tests\User\Repository;

use PHPUnit\Framework\TestCase;
use HR\User\Entity\User;
use HR\User\Repository\UserRepository;
use \Tests\User\Repository\ClientMockTrait;

class UserRepositoryTest extends TestCase
{
    use ClientMockTrait;

    public function testInitial(): void
    {
        $repository = new UserRepository($this->getClientMock());
        $this->assertInstanceOf(UserRepository::class, $repository);
    }

    public function testFind(): void
    {
        $repository = new UserRepository($this->getClientMock());
        $return = $repository->find();

        $this->assertInternalType('array', $return);
    }

    public function testPersist(): void
    {
        $repository = new UserRepository($this->getClientMock());

        $user = new User('test', 'test', 'test', 'test');
        $inserted = $repository->persist($user);

        $this->assertTrue($inserted);
    }
}
