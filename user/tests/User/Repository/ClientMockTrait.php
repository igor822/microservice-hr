<?php declare(strict_types=1);

namespace Tests\User\Repository;

use MongoDB\Database;
use MongoDB\Collection;
use MongoDB\InsertOneResult;

trait ClientMockTrait
{
    public function getClientMock()
    {
        $mock = $this->createMock(Database::class);
        $mock->expects($this->any())
             ->method('selectCollection')
             ->willReturn($this->getClientCollectionMock());

        return $mock;
    }

    public function getClientCollectionMock()
    {
        $collectionMock = $this->createMock(Collection::class);
        $collectionMock->expects($this->any())
                       ->method('find')
                       ->willReturn([]);

        $collectionMock->expects($this->any())
                       ->method('insertOne')
                       ->willReturn($this->getInsertOneResultMock());

        return $collectionMock;
    }

    public function getInsertOneResultMock()
    {
        $resultMock = $this->createMock(InsertOneResult::class);
        $resultMock->expects($this->any())
                   ->method('getInsertedCount')
                   ->willReturn(1);

        return $resultMock;
    }
}
