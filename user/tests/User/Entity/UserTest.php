<?php declare(strict_types=1);

namespace Tests\User\Entity;

use PHPUnit\Framework\TestCase;
use HR\User\Entity\User;

class UserTest extends TestCase
{
    public function testInitial()
    {
        $user = new User('test', 'test', 'test', 'test');

        $this->assertInstanceOf(User::class, $user);
    }

    public function testValidationData()
    {
        $firstName = 'First Name';
        $lastName = 'Last Name';
        $email = 'email@email.com';
        $companyId = 1;

        $user = new User($firstName, $lastName, $email, $companyId);

        $this->assertEquals($firstName, $user->getFirstName());
        $this->assertEquals($lastName, $user->getLastName());
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals($companyId, $user->getCompanyId());
    }
}
