<?php declare(strict_types=1);

namespace HR\User\Repository;

use MongoDB\Database;
use HR\User\Entity\User;

class UserRepository
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function find(array $filter = []): array
    {
        $db = $this->database->selectCollection('user');
        $cursor = $db->find();
        $data = [];
        foreach ($cursor as $item) {
            $data[] = (array) $item;
        }
        return $data;
    }

    public function persist(User $user): bool
    {
        $db = $this->database->selectCollection('user');
        $data = $db->insertOne($user->toArray());
        return boolval($data->getInsertedCount());        
    }
}
