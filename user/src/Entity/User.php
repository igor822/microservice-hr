<?php declare(strict_types=1);

namespace HR\User\Entity;

class User
{
    private $firstName;

    private $lastName;

    private $email;

    private $login;

    private $password;

    private $companyId;

    public function __construct($firstName, $lastName, $email, $companyId)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->companyId = $companyId;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function toArray(): array
    {
        return [
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'email' => $this->getEmail(),
            'companyId' => $this->getCompanyId()
        ];
    }
}
