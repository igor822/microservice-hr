<?php

namespace HR\User\Service;

use GuzzleHttp\Client;

class CompanyService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function companyExists($companyId): bool
    {
        $response = $this->client->request('GET', 'http://localhost:1881/companies/' . $companyId);
        $data = json_decode($response->getBody(), true);
        if (isset($data['error'])) {
            return false;
        }

        if (!count($data)) {
            return false;
        }

        return true;
    }
}
