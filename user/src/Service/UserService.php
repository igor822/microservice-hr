<?php declare(strict_types=1);

namespace HR\User\Service;

use HR\User\Repository\UserRepository;
use HR\User\Entity\User;
use MongoDB\Database;

class UserService
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function loadAll(): array
    {
        $repository = new UserRepository($this->database);
        return $repository->find();
    }

    public function storeNewUser(User $user): void
    {
        $companyService = new CompanyService();
        if ($companyService->companyExists($user->getCompanyId())) {
            $repository = new UserRepository($this->database);
            $repository->persist($user); 
        }
    }
}
