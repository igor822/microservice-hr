<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use HR\User\Entity\User;
use HR\User\Service\UserService;
use MongoDB\Client;

require 'vendor/autoload.php';

$mongoClient = function() {
    $client = new Client();

    return $client->selectDatabase('hr_user');
};

$app = new \Slim\App;
$app->post('/users', function (Request $request, Response $response) use ($mongoClient) {
    $body = $request->getParsedBody();

    $user = new User($body['firstName'], $body['lastName'], $body['email'], $body['companyId']);
    $service = new UserService($mongoClient());
    $service->storeNewUser($user);
});

$app->get('/users', function (Request $request, Response $response) use ($mongoClient) {
    $service = new UserService($mongoClient());
    $data = $service->loadAll();

    $response->getBody()->write(json_encode($data));
    return $response;
});
$app->run();
