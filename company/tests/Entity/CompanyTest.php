<?php declare(strict_types=1);

namespace Tests\Company\Entity;

use PHPUnit\Framework\TestCase;
use HR\Company\Entity\Company;

class CompanyTest extends TestCase
{
    public function testInitial()
    {
        $company = new Company('test', 10, true);

        $this->assertInstanceOf(Company::class, $company);
    }

    public function testValidationData()
    {
        $name = 'Name';
        $numberOfEmploees = 10;
        $active = false;

        $company = new Company($name, $numberOfEmploees, $active);

        $this->assertEquals($name, $company->getName());
        $this->assertEquals($numberOfEmploees, $company->getNumberOfEmploees());
        $this->assertEquals($active, $company->getActive());

        $this->assertInternalType('string', $company->getName());
        $this->assertInternalType('int', $company->getNumberOfEmploees());
        $this->assertFalse($company->getActive());
    }
}
