<?php declare(strict_types=1);

namespace Tests\Company\Repository;

use PHPUnit\Framework\TestCase;
use HR\Company\Entity\Company;
use HR\Company\Repository\CompanyRepository;

class CompanyRepositoryTest extends TestCase
{
    use ClientMockTrait;

    public function testInitial(): void
    {
        $repository = new CompanyRepository($this->getClientMock());
        $this->assertInstanceOf(CompanyRepository::class, $repository);
    }

    public function testFind(): void
    {
        $repository = new CompanyRepository($this->getClientMock());
        $return = $repository->find();

        $this->assertInternalType('array', $return);
    }

    public function testPersist(): void
    {
        $repository = new CompanyRepository($this->getClientMock());

        $company = new Company('test', 10, true);
        $inserted = $repository->persist($company);

        $this->assertTrue($inserted);
    }
}
