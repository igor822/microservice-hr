<?php declare(strict_types=1);

namespace HR\Company\Service;

use HR\Company\Repository\CompanyRepository;
use HR\Company\Entity\Company;
use MongoDB\Database;
use MongoDB\BSON\ObjectID;

class CompanyService
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function loadAll(): array
    {
        $repository = new CompanyRepository($this->database);
        return $repository->find();
    }

    public function findById($id): array
    {
        try {
            $id = new ObjectID($id);
        } catch (\Exception $e) {
            throw $e;
        }
        $repository = new CompanyRepository($this->database);
        return $repository->findOneBy('_id', $id);
    }

    public function storeNewCompany(Company $company): void
    {
        $repository = new CompanyRepository($this->database);
        $repository->persist($company); 
    }
}
