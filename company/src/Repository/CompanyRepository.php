<?php declare(strict_types=1);

namespace HR\Company\Repository;

use MongoDB\Database;
use HR\Company\Entity\Company;

class CompanyRepository
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function find(array $filter = []): array
    {
        $db = $this->database->selectCollection('company');
        $cursor = $db->find();
        $data = [];
        foreach ($cursor as $item) {
            $data[] = (array) $item;
        }
        return $data;
    }

    public function findOneBy(string $index, $value): array
    {
        $db = $this->database->selectCollection('company');
        $cursor = $db->findOne([$index => $value]);

        return (array) $cursor;
    }

    public function persist(Company $company): bool
    {
        $db = $this->database->selectCollection('company');
        $data = $db->insertOne($company->toArray());
        return boolval($data->getInsertedCount());        
    }
}
