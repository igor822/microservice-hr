<?php declare(strict_types=1);

namespace HR\Company\Entity;

class Company
{
    private $name;

    private $active;

    private $numberOfEmploees;

    public function __construct(string $name, int $numberOfEmploees, bool $active = true)
    {
        $this->name = $name;
        $this->numberOfEmploees = $numberOfEmploees;
        $this->active = $active;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getNumberOfEmploees(): int
    {
        return $this->numberOfEmploees;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'active' => $this->getActive(),
            'numberOfEmploees' => $this->getNumberOfEmploees()
        ];
    }
}
