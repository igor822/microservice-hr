<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use HR\Company\Entity\Company;
use HR\Company\Service\CompanyService;
use MongoDB\Client;

require 'vendor/autoload.php';

$mongoClient = function() {
    $client = new Client();

    return $client->selectDatabase('hr_company');
};

$app = new \Slim\App;
$app->post('/companies', function (Request $request, Response $response) use ($mongoClient) {
    $body = $request->getParsedBody();

    $company = new Company($body['name'], $body['numberOfEmployees'], boolval($body['active']));
    $service = new CompanyService($mongoClient());
    $service->storeNewCompany($company);
});

$app->get('/companies', function (Request $request, Response $response) use ($mongoClient) {
    $service = new CompanyService($mongoClient());
    $data = $service->loadAll();

    $response->getBody()->write(json_encode($data));
    return $response;
});

$app->get('/companies/{id}', function (Request $request, Response $response, array $args) use ($mongoClient) {
    try {
        $id = $args['id'];
        $service = new CompanyService($mongoClient());
        $data = $service->findById($id);
    } catch (\Exception $e) {
        $response->getBody()->write(json_encode(['message' => $e->getMessage(), 'error' => 1]));
        return $response;
    }

    $response->getBody()->write(json_encode($data));
    return $response;
});
$app->run();
